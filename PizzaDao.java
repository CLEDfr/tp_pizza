package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.transaction.Transaction;
import java.util.Iterator;
import java.util.UUID;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

public interface PizzaDao
{
    @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL, base VARCHAR, price_small FLOAT, price_large FLOAT, image VARCHAR)")
    void createPizzaTable();
    
    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (id_pizza VARCHAR(128) NOT NULL, id_ingredient VARCHAR(128) NOT NULL)")
    void createAssociationTable();
    
    default void createTableAndIngredientAssociation() {
        this.createAssociationTable();
        this.createPizzaTable();
    }
    
    @Transaction
    default void insertWithAssociation(final Pizza pizza) {
        this.insert(pizza);
        final UUID id = pizza.getId();
        for (final Ingredient ingredient : pizza.getIngredients()) {
            this.insertAssociation(id, ingredient.getId());
        }
    }
    
    @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
    void dropPizzaTable();
    
    @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
    void dropAssociationTable();
    
    @Transaction
    default void dropPizzaAndIngredientAssociation() {
        this.dropAssociationTable();
        this.dropPizzaTable();
    }
    
    @SqlUpdate("INSERT INTO Pizzas (id, name, base, price_small, price_large, image) values (:id, :name, :base, :price_small, :price_large, :image)")
    @GetGeneratedKeys
    long insert(@BindBean final Pizza p0);
    
    @SqlUpdate("INSERT INTO PizzaIngredientsAssociation (id_pizza, id_ingredient) values (:id_pizza, :id_ingredient)")
    void insertAssociation(@Bind("id_pizza") final UUID p0, @Bind("id_ingredient") final UUID p1);
    
    @SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") final String p0);
    
    @SqlQuery("SELECT * FROM Pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();
    
    @SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") final UUID p0);
    
    @Transaction
    default void deletePizzaWithAssociation(final UUID id) {
        this.deletePizza(id);
        this.deletePizzaAssociation(id);
    }
    
    @SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
    void deletePizza(@Bind("id") final UUID p0);
    
    @SqlUpdate("DELETE FROM PizzaIngredientsAssociation WHERE id_pizza = :id")
    void deletePizzaAssociation(@Bind("id") final UUID p0);
    
    @SqlQuery("SELECT * FROM ingredients WHERE id IN (SELECT id_ingredient FROM PizzaIngredientsAssociation WHERE id_pizza = :id)")
    @RegisterBeanMapper(Ingredient.class)
    List<Ingredient> getIngredientsPizza(@Bind("id") final UUID p0);
}

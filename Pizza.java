package fr.ulille.iut.pizzaland.beans;

import java.util.stream.Collector;
import java.util.stream.Collectors;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaShortDto;
import java.util.List;
import java.util.UUID;

public class Pizza
{
    private UUID id;
    private String name;
    private String base;
    private float price_small;
    private float price_large;
    private String image;
    private List<Ingredient> ingredients;
    
    public Pizza() {
        this.id = UUID.randomUUID();
    }
    
    public Pizza(final String name, final String base, final List<Ingredient> ingredients, final float price_small, final float price_large, final String image) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.base = base;
        this.ingredients = ingredients;
        this.price_small = price_small;
        this.price_large = price_large;
        this.image = image;
    }
    
    public List<Ingredient> getIngredients() {
        return this.ingredients;
    }
    
    public void setIngredients(final List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
    
    public boolean addIngredient(final Ingredient ingredient) {
        if (this.ingredients.contains(ingredient)) {
            return false;
        }
        this.ingredients.add(ingredient);
        return true;
    }
    
    public boolean removeIngredient(final Ingredient ingredient) {
        if (!this.ingredients.contains(ingredient)) {
            return false;
        }
        this.ingredients.remove(ingredient);
        return true;
    }
    
    public String getImage() {
        return this.image;
    }
    
    public void setImage(final String image) {
        this.image = image;
    }
    
    public float getPrice_large() {
        return this.price_large;
    }
    
    public void setPrice_large(final float price_large) {
        this.price_large = price_large;
    }
    
    public float getPrice_small() {
        return this.price_small;
    }
    
    public void setPrice_small(final float price_small) {
        this.price_small = price_small;
    }
    
    public String getBase() {
        return this.base;
    }
    
    public void setBase(final String base) {
        this.base = base;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public UUID getId() {
        return this.id;
    }
    
    public void setId(final UUID id) {
        this.id = id;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + ((this.base == null) ? 0 : this.base.hashCode());
        result = 31 * result + ((this.id == null) ? 0 : this.id.hashCode());
        result = 31 * result + ((this.image == null) ? 0 : this.image.hashCode());
        result = 31 * result + ((this.ingredients == null) ? 0 : this.ingredients.hashCode());
        result = 31 * result + ((this.name == null) ? 0 : this.name.hashCode());
        result = 31 * result + Float.floatToIntBits(this.price_large);
        result = 31 * result + Float.floatToIntBits(this.price_small);
        return result;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Pizza other = (Pizza)obj;
        if (this.base == null) {
            if (other.base != null) {
                return false;
            }
        }
        else if (!this.base.equals(other.base)) {
            return false;
        }
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        }
        else if (!this.id.equals(other.id)) {
            return false;
        }
        if (this.image == null) {
            if (other.image != null) {
                return false;
            }
        }
        else if (!this.image.equals(other.image)) {
            return false;
        }
        if (this.ingredients == null) {
            if (other.ingredients != null) {
                return false;
            }
        }
        else if (!this.ingredients.equals(other.ingredients)) {
            return false;
        }
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        }
        else if (!this.name.equals(other.name)) {
            return false;
        }
        return Float.floatToIntBits(this.price_large) == Float.floatToIntBits(other.price_large) && Float.floatToIntBits(this.price_small) == Float.floatToIntBits(other.price_small);
    }
    
    @Override
    public String toString() {
        return invokedynamic(makeConcatWithConstants:(Ljava/lang/String;Ljava/util/UUID;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;FF)Ljava/lang/String;, this.base, this.id, this.ingredients, this.name, this.image, this.price_large, this.price_small);
    }
    
    public static PizzaShortDto toShortDto(final Pizza pizza) {
        final PizzaShortDto dto = new PizzaShortDto();
        dto.setId(pizza.getId());
        dto.setName(pizza.getName());
        dto.setBase(pizza.getBase());
        dto.setPrice_large(pizza.getPrice_large());
        dto.setPrice_small(pizza.getPrice_small());
        dto.setImage(pizza.getImage());
        return dto;
    }
    
    public static Pizza fromShortDto(final PizzaShortDto dto) {
        final Pizza pizza = new Pizza();
        pizza.setName(dto.getName());
        pizza.setBase(dto.getBase());
        pizza.setPrice_large(dto.getPrice_large());
        pizza.setPrice_small(dto.getPrice_small());
        pizza.setImage(dto.getImage());
        return pizza;
    }
    
    public static Pizza fromCreateDto(final PizzaCreateDto dto) {
        final Pizza pizza = new Pizza();
        if (dto.getId() != null) {
            pizza.setId(dto.getId());
        }
        pizza.setName(dto.getName());
        pizza.setBase(dto.getBase());
        pizza.setPrice_large(dto.getPrice_large());
        pizza.setPrice_small(dto.getPrice_small());
        pizza.setImage(dto.getImage());
        return pizza;
    }
    
    public static PizzaCreateDto toCreateDto(final Pizza pizza) {
        final PizzaCreateDto dto = new PizzaCreateDto();
        dto.setId(pizza.getId());
        dto.setBase(pizza.getBase());
        dto.setName(pizza.getName());
        dto.setPrice_large(pizza.getPrice_large());
        dto.setPrice_small(pizza.getPrice_small());
        dto.setImage(pizza.getImage());
        final List<UUID> ingredientIds = pizza.getIngredients().stream().map(ingredient -> ingredient.getId()).collect((Collector<? super Object, ?, List<UUID>>)Collectors.toList());
        dto.setIngredients(ingredientIds);
        return dto;
    }
}

package fr.ulille.iut.pizzaland;

import java.util.Iterator;
import java.util.UUID;
import java.util.ArrayList;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import java.io.Reader;
import jakarta.json.bind.JsonbBuilder;
import java.util.List;
import java.io.InputStreamReader;
import java.util.logging.Logger;
import jakarta.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("api/v1/")
public class ApiV1 extends ResourceConfig
{
    private static final Logger LOGGER;
    
    public ApiV1() {
        this.packages(new String[] { "fr.ulille.iut.pizzaland" });
        final String environment = System.getenv("PIZZAENV");
        if (environment != null && environment.equals("withdb")) {
            ApiV1.LOGGER.info("Loading with database");
            try {
                InputStreamReader reader = new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("ingredients.json"));
                final List<Ingredient> ingredients = (List<Ingredient>)JsonbBuilder.create().fromJson((Reader)reader, new ApiV1.ApiV1$1(this).getClass().getGenericSuperclass());
                final IngredientDao ingredientDao = (IngredientDao)BDDFactory.buildDao((Class)IngredientDao.class);
                ingredientDao.dropTable();
                ingredientDao.createTable();
                for (final Ingredient ingredient : ingredients) {
                    ingredientDao.insert(ingredient);
                }
                reader = new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("pizzas.json"));
                final List<PizzaCreateDto> pizzaCreates = (List<PizzaCreateDto>)JsonbBuilder.create().fromJson((Reader)reader, new ApiV1.ApiV1$2(this).getClass().getGenericSuperclass());
                ApiV1.LOGGER.info(pizzaCreates.toString());
                final PizzaDao pizzaDao = (PizzaDao)BDDFactory.buildDao((Class)PizzaDao.class);
                pizzaDao.dropPizzaAndIngredientAssociation();
                pizzaDao.createTableAndIngredientAssociation();
                for (final PizzaCreateDto pizzaCreate : pizzaCreates) {
                    final Pizza pizza = Pizza.fromCreateDto(pizzaCreate);
                    final ArrayList<Ingredient> ingredientsList = new ArrayList<Ingredient>();
                    for (final UUID id : pizzaCreate.getIngredients()) {
                        ingredientsList.add(ingredientDao.findById(id));
                    }
                    pizza.setIngredients(ingredientsList);
                    pizzaDao.insertWithAssociation(pizza);
                }
            }
            catch (Exception ex) {
                throw new IllegalStateException(ex);
            }
        }
    }
    
    static {
        LOGGER = Logger.getLogger(ApiV1.class.getName());
    }
}

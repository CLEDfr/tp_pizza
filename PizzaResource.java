package fr.ulille.iut.pizzaland.resources;

import fr.ulille.iut.pizzaland.dto.IngredientDto;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.POST;
import java.net.URI;
import java.util.Iterator;
import java.util.UUID;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import java.util.ArrayList;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import jakarta.ws.rs.GET;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dto.PizzaShortDto;
import java.util.List;
import fr.ulille.iut.pizzaland.BDDFactory;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import java.util.logging.Logger;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.Path;

@Path("pizzas")
@Produces({ "application/json" })
public class PizzaResource
{
    private static final Logger LOGGER;
    private PizzaDao pizzas;
    private IngredientDao ingredients;
    @Context
    public UriInfo uriInfo;
    
    public PizzaResource() {
        this.pizzas = (PizzaDao)BDDFactory.buildDao((Class)PizzaDao.class);
        this.ingredients = (IngredientDao)BDDFactory.buildDao((Class)IngredientDao.class);
        this.pizzas.createTableAndIngredientAssociation();
    }
    
    @GET
    public List<PizzaShortDto> getAll() {
        final List<PizzaShortDto> l = (List<PizzaShortDto>)this.pizzas.getAll().stream().map(Pizza::toShortDto).collect(Collectors.toList());
        return l;
    }
    
    @POST
    public Response createPizza(final PizzaCreateDto pizzaCreateDto) {
        final Pizza existing = this.pizzas.findByName(pizzaCreateDto.getName());
        if (existing != null) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }
        final Pizza pizza = Pizza.fromCreateDto(pizzaCreateDto);
        final ArrayList<Ingredient> ingredientsList = new ArrayList<Ingredient>();
        for (final UUID id : pizzaCreateDto.getIngredients()) {
            try {
                ingredientsList.add(this.ingredients.findById(id));
            }
            catch (Exception e2) {
                throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
            }
        }
        pizza.setIngredients(ingredientsList);
        try {
            this.pizzas.insertWithAssociation(pizza);
            pizzaCreateDto.setId(pizza.getId());
            final URI uri = this.uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build(new Object[0]);
            return Response.created(uri).entity((Object)pizzaCreateDto).build();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }
    
    @GET
    @Path("{id}")
    public PizzaShortDto getOnePizza(@PathParam("id") final UUID id) {
        final Pizza pizza = this.pizzas.findById(id);
        if (pizza == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return Pizza.toShortDto(pizza);
    }
    
    @DELETE
    @Path("{id}")
    public Response deletePizza(@PathParam("id") final UUID id) {
        final Pizza pizza = this.pizzas.findById(id);
        if (pizza == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        this.pizzas.deletePizzaWithAssociation(id);
        return Response.accepted().build();
    }
    
    @GET
    @Path("{id}/ingredients")
    public List<IngredientDto> getPizzaIngredients(@PathParam("id") final UUID id) {
        final List<Ingredient> ingredients = (List<Ingredient>)this.pizzas.getIngredientsPizza(id);
        if (ingredients == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        PizzaResource.LOGGER.info(ingredients.toString());
        return ingredients.stream().map((Function<? super Object, ?>)Ingredient::toDto).collect((Collector<? super Object, ?, List<IngredientDto>>)Collectors.toList());
    }
    
    static {
        LOGGER = Logger.getLogger(IngredientResource.class.getName());
    }
}

package fr.ulille.iut.pizzaland.dto;

import java.util.UUID;

public class PizzaShortDto
{
    private UUID id;
    private String name;
    private String base;
    private float price_small;
    private float price_large;
    private String image;
    
    public UUID getId() {
        return this.id;
    }
    
    public void setId(final UUID id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getBase() {
        return this.base;
    }
    
    public void setBase(final String base) {
        this.base = base;
    }
    
    public float getPrice_small() {
        return this.price_small;
    }
    
    public void setPrice_small(final float price_small) {
        this.price_small = price_small;
    }
    
    public float getPrice_large() {
        return this.price_large;
    }
    
    public void setPrice_large(final float price_large) {
        this.price_large = price_large;
    }
    
    public String getImage() {
        return this.image;
    }
    
    public void setImage(final String pizza_img_url) {
        this.image = pizza_img_url;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + ((this.base == null) ? 0 : this.base.hashCode());
        result = 31 * result + ((this.id == null) ? 0 : this.id.hashCode());
        result = 31 * result + ((this.image == null) ? 0 : this.image.hashCode());
        result = 31 * result + ((this.name == null) ? 0 : this.name.hashCode());
        result = 31 * result + Float.floatToIntBits(this.price_large);
        result = 31 * result + Float.floatToIntBits(this.price_small);
        return result;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final PizzaShortDto other = (PizzaShortDto)obj;
        if (this.base == null) {
            if (other.base != null) {
                return false;
            }
        }
        else if (!this.base.equals(other.base)) {
            return false;
        }
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        }
        else if (!this.id.equals(other.id)) {
            return false;
        }
        if (this.image == null) {
            if (other.image != null) {
                return false;
            }
        }
        else if (!this.image.equals(other.image)) {
            return false;
        }
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        }
        else if (!this.name.equals(other.name)) {
            return false;
        }
        return Float.floatToIntBits(this.price_large) == Float.floatToIntBits(other.price_large) && Float.floatToIntBits(this.price_small) == Float.floatToIntBits(other.price_small);
    }
    
    @Override
    public String toString() {
        return invokedynamic(makeConcatWithConstants:(Ljava/lang/String;Ljava/util/UUID;Ljava/lang/String;Ljava/lang/String;FF)Ljava/lang/String;, this.base, this.id, this.name, this.image, this.price_large, this.price_small);
    }
}
